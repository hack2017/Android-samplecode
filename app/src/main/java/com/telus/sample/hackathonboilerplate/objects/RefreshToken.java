package com.telus.sample.hackathonboilerplate.objects;

import org.json.JSONException;

/**
 * Created by garett on 2016-10-27.
 */

public class RefreshToken extends TokenBase {
    public RefreshToken(String tokenString) throws JSONException, InstantiationException {
        super(tokenString);
    }
}
