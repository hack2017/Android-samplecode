package com.telus.sample.hackathonboilerplate.objects;

import org.json.JSONObject;

/**
 * Created by garett on 2016-12-21.
 */

public class UserProfile {
    /**
     * "lastName": "Admin",
     * "firstName": "App",
     * "email": null
     */

    private String firstName;

    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public static UserProfile parseFromJson(JSONObject jsonUserProfile) {
        if (jsonUserProfile == null){
            return null;
        }
        UserProfile userProfile = new UserProfile();
        userProfile.firstName = jsonUserProfile.optString("firstName");
        userProfile.lastName = jsonUserProfile.optString("lastName");
        return userProfile;
    }
}
