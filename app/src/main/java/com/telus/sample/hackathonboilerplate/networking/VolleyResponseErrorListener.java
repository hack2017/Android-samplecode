package com.telus.sample.hackathonboilerplate.networking;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by garett on 2017-01-16.
 */

public class VolleyResponseErrorListener implements Response.ErrorListener {

    private static List<Response.ErrorListener> mResponseErrorListenerObserverList = new ArrayList<>();
    private static VolleyResponseErrorListener mVolleyResponseErrorListener;

    public static VolleyResponseErrorListener getInstance(){
        if (mVolleyResponseErrorListener == null){
            mVolleyResponseErrorListener = new VolleyResponseErrorListener();
        }
        return mVolleyResponseErrorListener;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        for (Response.ErrorListener errorListener : mResponseErrorListenerObserverList){
            errorListener.onErrorResponse(error);
        }
        if (error instanceof TimeoutError) {
        } else if (error instanceof NoConnectionError) {
        } else if (error instanceof AuthFailureError) {
            //TODO
        } else if (error instanceof ServerError) {
            //TODO
        } else if (error instanceof NetworkError) {
            //TODO
        } else if (error instanceof ParseError) {
            //TODO
        }

    }

    public static void addListener(Response.ErrorListener newErrorListener){
        if (mResponseErrorListenerObserverList == null){
            mResponseErrorListenerObserverList = new ArrayList<>();
        }
        if (!mResponseErrorListenerObserverList.contains(newErrorListener)){
            mResponseErrorListenerObserverList.add(newErrorListener);
        }
    }

    public static void removeListener(Response.ErrorListener errorListenerToRemove){
        if (mResponseErrorListenerObserverList.contains(errorListenerToRemove)){
            mResponseErrorListenerObserverList.remove(errorListenerToRemove);
        }
    }

}
