package com.telus.sample.hackathonboilerplate.networking;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by garett on 2016-10-18.
 */

class AuthenticatedJsonObjectRequest extends JsonObjectRequest {

    private String AUTH_TOKEN;

    AuthenticatedJsonObjectRequest(int method, String url, JSONObject jsonRequest, String authToken, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        super(method, url, jsonRequest, listener, errorListener);
        AUTH_TOKEN = authToken;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.putAll(super.getHeaders());
        if (AUTH_TOKEN != null){
            headers.put("Authorization", "Bearer " + AUTH_TOKEN);
        }
        return headers;
    }
}
