package com.telus.sample.hackathonboilerplate;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DataPullService {

    public static String pull(String token, String endpoint){

        URL url;
        HttpURLConnection urlConnection;
        try {
            url = new URL(endpoint);
            urlConnection = (HttpURLConnection) url.openConnection();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        try {

            urlConnection.setRequestProperty("Authorization", "Bearer "  + token);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.connect();

            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.d("urlConnection", "" + urlConnection.getResponseCode());
                InputStream in = new BufferedInputStream(urlConnection.getErrorStream());
                StringBuilder res = new StringBuilder();
                int readChar;
                while ((readChar = in.read()) != -1) {
                    res.append((char) readChar);
                }
                Log.e("HTTP ERROR", res.toString());
                return null;
            }

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            StringBuilder res = new StringBuilder();
            int readChar;
            while ((readChar = in.read()) != -1) {
                res.append((char) readChar);
            }
            Log.d("RESPONSE", res.toString());
            return res.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        return null;
    }
}
