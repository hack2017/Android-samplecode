package com.telus.sample.hackathonboilerplate;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


class LoginService {

    private static final String LOGIN_JSON_TEMPLATE = "{\"username\":\"$$$username$$$\", \"password\":\"$$$password$$$\"}";
    private static final String USERNAME_KEY = "$$$username$$$";
    private static final String PASSWORD_KEY = "$$$password$$$";
    private static final String JSON_TOKEN_ID = "accessToken";

    static String login(final String username, final String password, String endpoint) {

        URL url;
        HttpURLConnection urlConnection;
        try {
            url = new URL(endpoint);
            urlConnection = (HttpURLConnection) url.openConnection();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        try {
            String loginString = LOGIN_JSON_TEMPLATE.replace(USERNAME_KEY, username).replace(PASSWORD_KEY, password);

            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setDoOutput(true);

            BufferedOutputStream bos = new BufferedOutputStream(urlConnection.getOutputStream());
            bos.write(loginString.getBytes());
            bos.flush();
            bos.close();

            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.d("urlConnection", "" + urlConnection.getResponseCode());
                InputStream in = new BufferedInputStream(urlConnection.getErrorStream());
                StringBuilder res = new StringBuilder();
                int readChar;
                while ((readChar = in.read()) != -1) {
                    res.append((char) readChar);
                }
                Log.e("HTTP ERROR", res.toString());
                return null;
            }

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            StringBuilder res = new StringBuilder();
            int readChar;
            while ((readChar = in.read()) != -1) {
                res.append((char) readChar);
            }
            JSONObject serverResponseJson = new JSONObject(res.toString());
            String token = serverResponseJson.get(JSON_TOKEN_ID).toString();
            Log.d(JSON_TOKEN_ID, token);
            return token;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        return null;

    }

}
