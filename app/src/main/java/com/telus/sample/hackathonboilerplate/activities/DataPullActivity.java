package com.telus.sample.hackathonboilerplate.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.telus.sample.hackathonboilerplate.AuthManager;
import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.networking.ControlPanel;
import com.telus.sample.hackathonboilerplate.objects.AuthToken;

import org.json.JSONObject;

public class DataPullActivity extends AppCompatActivity {

    private String mToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_pull);

        final AuthToken authToken = AuthManager.getInstance().getAuthToken();
        mToken = authToken.token;


        findViewById(R.id.pullDataButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControlPanel.getContactList(mToken, authToken.userid, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        updateDataDumpField(response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        updateDataDumpField(error.getMessage());
                    }
                });
            }
        });

    }

    private void updateDataDumpField(final String message){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView)findViewById(R.id.dataDump)).setText(message);
            }
        });
    }

}
