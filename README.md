# Android Basic sample app

This app is a starting point for an user wanting to use the hackathon backend on Android. A few components have been implemented for a developer to pick up and extend, or use as reference.

## Modules

This project uses an additional module for networking to simplify API calls, check the app module's build.gradle file for more details on how those are added:

- com.android.volley - Easy networking module; you volley some data to the server, it volleys back a response

## Components in the project

- Authentication (basic username and password)
- JWT reading
- API call with token
- Token storage and purging
- Local crypto functions

## Structure

- activities/ - All screens users can interact with, paired with layouts in /res
- crypto/ - Minimalist cryptography module, handles encryption/decryption, hashing and IV generation
- networking/ - Handles all app networking logic
- networking/ControlPanel.java - Interface for making calls to the API server
- objects/ - AES Encryption and Decryption - extends String to be used as string.encrypt
- AuthManager.java - Orchestrates token management
- DataStore.java - Local data storage, only really suitable for small amounts of data (e.g. settings)


